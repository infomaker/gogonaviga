// Protocol Buffers for Go with Gadgets
//
// Copyright (c) 2015, The GoGo Authors. All rights reserved.
// http://github.com/gogo/protobuf
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

package main

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"github.com/gogo/protobuf/gogoproto"
	"github.com/gogo/protobuf/proto"
	"github.com/gogo/protobuf/protoc-gen-gogo/descriptor"
	"github.com/gogo/protobuf/vanity"
	"github.com/gogo/protobuf/vanity/command"
)

func main() {
	req := command.Read()
	files := req.GetProtoFile()
	files = vanity.FilterFiles(files, vanity.NotGoogleProtobufDescriptorProto)

	vanity.ForEachFile(files, vanity.TurnOnMarshalerAll)
	vanity.ForEachFile(files, vanity.TurnOnSizerAll)
	vanity.ForEachFile(files, vanity.TurnOnUnmarshalerAll)

	vanity.ForEachFieldInFilesExcludingExtensions(vanity.OnlyProto2(files), vanity.TurnOffNullableForNativeTypesWithoutDefaultsOnly)
	vanity.ForEachFile(files, vanity.TurnOffGoUnrecognizedAll)
	vanity.ForEachFile(files, vanity.TurnOffGoUnkeyedAll)
	vanity.ForEachFile(files, vanity.TurnOffGoSizecacheAll)

	vanity.ForEachFile(files, vanity.TurnOffGoEnumPrefixAll)
	vanity.ForEachFile(files, vanity.TurnOffGoEnumStringerAll)
	vanity.ForEachFile(files, vanity.TurnOnEnumStringerAll)

	vanity.ForEachFile(files, vanity.TurnOnEqualAll)
	vanity.ForEachFile(files, vanity.TurnOnGoStringAll)
	vanity.ForEachFile(files, vanity.TurnOffGoStringerAll)
	vanity.ForEachFile(files, vanity.TurnOnStringerAll)

	parameters := parseParameters(*req.Parameter)

	if _, ok := parameters["use_stdtime"]; ok {
		// Use stdlib time.Time for google.protobuf.Timestamp.
		useTime := vanity.SetBoolFieldOption(gogoproto.E_Stdtime, true)
		vanity.ForEachFieldInFiles(files, func(field *descriptor.FieldDescriptorProto) {
			if field.GetTypeName() == ".google.protobuf.Timestamp" {
				useTime(field)
			}
		})
	}

	var idiomaticMappings map[string]string

	// Set up some standard mappings for acronyms and initialisms.
	if _, ok := parameters["default_custom_names"]; ok {
		idiomaticMappings = map[string]string{
			"uuid": "UUID",
			"id":   "ID",
			"url":  "URL",
			"uri":  "URI",
			"http": "HTTP",
		}
	}

	// Read custom names from file
	if filename, ok := parameters["custom_names"]; ok {
		file, err := os.Open(filename)
		if err != nil {
			fmt.Fprintf(os.Stderr,
				"failed to open custom field name file %q: %s",
				filename, err.Error())
			os.Exit(1)
		}
		defer file.Close()

		dec := json.NewDecoder(file)
		if err := dec.Decode(&idiomaticMappings); err != nil {
			fmt.Fprintf(os.Stderr,
				"failed to parse custom field name file %q: %s",
				filename, err.Error())
			os.Exit(1)
		}
	}

	vanity.ForEachMessageInFiles(files, func(msg *descriptor.DescriptorProto) {
		for _, field := range msg.GetField() {
			qName := *msg.Name + "." + *field.Name
			name, mapName := idiomaticMappings[qName]
			if !mapName {
				name, mapName = idiomaticMappings[*field.Name]
			}

			explicitOpt := proto.HasExtension(
				field.Options, gogoproto.E_Customname)

			if mapName && !explicitOpt {
				if field.Options == nil {
					field.Options = &descriptor.FieldOptions{}
				}

				err := proto.SetExtension(
					field.Options,
					gogoproto.E_Customname, &name,
				)
				if err != nil {
					println(err.Error())
					os.Exit(1)
				}
			}
		}
	})

	resp := command.Generate(req)
	command.Write(resp)
}

func parseParameters(parameter string) map[string]string {
	param := make(map[string]string)
	for _, p := range strings.Split(parameter, ",") {
		if i := strings.Index(p, "="); i < 0 {
			param[p] = ""
		} else {
			param[p[0:i]] = p[i+1:]
		}
	}
	return param
}
