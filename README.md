## GoGoNaviga protobuf code generator

This is an extension of the gogoproto "protoc-gen-gogoslick" with
options for using `time.Time` for `google.protobuf.Timestamp` fields
and performing Go field name substitution using custom names.

These things are possible to do using normal gogo extensions, but then
we would litter the protobuf file with Go-specific options and
extensions, which not only feels like a bad thing to do, but also
would complicate things for non-Go consumers of the protobuf file.

Options:

- `use_std_time`: use `time.Time` for `google.protobuf.Timestamp`
  fields.
- `default_custom_names`: use a set of custom names that turns common
  acronyms and initialisms into uppercase (ID, UUID, URL, URI, HTTP).
- `custom_names=[filename.json]`: specifies a file containing
  additional custom names as a json objeect using the protobuf field
  name as the key and the custom name as value. Protobuf field names
  can be qualified with the message name `Message.field` and qualified
  names take precedence over unqualified names.

Example:

```
protoc --gogonaviga_out=\
	use_std_time,default_custom_names,\
	custom_names=map_file.json:out_dir \
	specfile.proto
```

